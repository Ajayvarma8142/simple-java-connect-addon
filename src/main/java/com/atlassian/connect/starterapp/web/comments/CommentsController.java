package com.atlassian.connect.starterapp.web.comments;

import java.util.List;
import java.util.Map;

import com.atlassian.connect.starterapp.domain.Comment;
import com.atlassian.connect.starterapp.service.CommentService;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/comments")
public class CommentsController {

    private final CommentService service;

    private final CommentsReact commentsReact;

    private final ObjectMapper mapper;

    @Autowired
    public CommentsController(CommentService service) {
        this.service = service;
        this.commentsReact = new CommentsReact();
        this.mapper = new ObjectMapper();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String comments(Map<String, Object> model) throws Exception {
        List<Comment> comments = service.getComments();
        String commentBox = commentsReact.renderCommentBox(comments);
        String data = mapper.writeValueAsString(comments);
        model.put("content", commentBox);
        model.put("data", data);
        return "comments";
    }
}
