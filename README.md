# Simple Java connect addon
This project demonstrates how to add Connect capabilities to an existing Java application using Spring Boot.
There is a blog post [here](https://developer.atlassian.com/blog/2017/03/connect-for-java/) describing how this is done.


# Requirements
* Maven 3 and above
* Java 8
* MySQL


## Quickstart
1. Install and set up MySQL. You may use the scripts provided in 'mysql' directory to do so.
1. Add an entry in your '/etc/hosts' file for 'mysql', pointing to your local machine (127.0.0.1).
1. Check src/main/resources/application.properties and make sure you have the right database configuration.
1. Run 'mvn clean install'.
1. Run ./run-local-server.sh script.
1. Access the application at http://localhost:8080.

## License
Copyright (c) 2016 Atlassian and others. Apache 2.0 licensed, see LICENSE.txt file.
